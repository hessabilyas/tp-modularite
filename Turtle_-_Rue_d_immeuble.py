import turtle as t 
import time
from typing import Tuple
from random import randint, shuffle

# Alias de types pour clarifier le code
Octet = int
Couleur = Tuple[Octet, Octet, Octet]
Coord = int


# constantes imposées
NIVEAU_MAX = 4
LARGEUR = 140
HAUT_NIV = 60
TAILLE_FEN = 30
LARG_PORTE = 30
NB_BAR = 5 # nb barreaux balcon



def ini_tortue(nb_maisons: int) -> None:
    """
    Place la tortue pour le début du dessin
    """
    t.setup(1.2*nb_maisons*LARGEUR, NIVEAU_MAX*1.2*HAUT_NIV)
    t.speed(0)
    t.hideturtle()

def rgb_to_hex(rgb):
    return '#%02x%02x%02x' % rgb

def couleur_aleatoire() -> Couleur:
    """
    Renvoie un triplet d'octets pour définir une couleur.
    """
    return rgb_to_hex((randint(0, 255),randint(0, 255),randint(0, 255)))

def trait(x1: Coord, y1: Coord, x2:Coord ,y2: Coord) -> None:
    '''
    Paramètres
        x1, y1 : coordonnées du début du trait
        x2, y2 : coordonnées de la fin du trait
    A la fin de la fonction le stylo est levé
    '''
    t.up()
    t.goto(x1, y2)
    t.down()
    t.goto(x2, y2)
    t.up()
    
def rectangle(x:Coord, y:Coord, w: Coord, h:Coord) -> None:
    '''
    Paramètres
        x, y : coordonnées du centre de la base de rectangle
        w : largeur du rectangle
        h : hauteur du rectangle
    '''
    t.up()
    t.goto(x - w / 2, y)
    t.setheading(0)
    t.down()
    for _ in range(2):
        t.forward(w)
        t.left(90)
        t.forward(h)
        t.left(90)

def fenetre(x:Coord, y:Coord) -> None:
    '''
    Paramètres :
        x est l'abcisse du centre de la fenêtre
        y est l'ordonnée du sol du niveau de la fenetre
    '''
    t.color('black', 'white')
    t.begin_fill()
    rectangle(x, y, TAILLE_FEN,TAILLE_FEN)
    t.end_fill()
    t.up()

def porte1(x:Coord, y:Coord, couleur:Couleur) -> None:
    '''
    Porte rectangulaire
    Paramètres :
        x est l'abcisse du centre de la porte
        y est l'ordonnée du sol du niveau de la porte
        couleur : couleur de la porte
    '''
    t.color('black', couleur)
    t.begin_fill()
    rectangle(x, y, LARG_PORTE, 50)
    t.end_fill()
    t.up()
        
def porte2(x:Coord, y:Coord, couleur:Couleur) -> None:
    '''
    Porte arondie
    Paramètres :
        x est l'abcisse du centre de la porte
        y est l'ordonnée du sol du niveau de la porte
        couleur : couleur de la porte
    '''
    t.color('black', couleur)
    t.up()
    t.goto(x - LARG_PORTE / 2, y)
    t.setheading(0)
    t.begin_fill()
    t.down()
    t.forward(LARG_PORTE)
    t.left(90)
    t.forward(35)
    t.circle(LARG_PORTE / 2, 180)
    t.forward(35)
    t.end_fill()

def portes() -> list:
    """
    Renvoie la liste des portes possibles
    """
    return [porte1, porte2]


# ----- Elements des étages : Balcon -----

def fenetre_balcon(x:Coord, y:Coord) -> None:
    '''
    Paramètres :
        x est l'abcisse du centre de la porte-fenetre-balcon
        y est l'ordonnée du sol du niveau de la porte-fenetre-balcon
    '''
    porte1(x, y, 'white')
    for k in range(NB_BAR):
        rectangle(x, y, (40 / NB_BAR + 2) * k, 30)

# ----- Facade avec : couleur + 3 élts d'étages -----

def facade(x:Coord, y_sol:Coord, couleur: Couleur, niveau: int) -> None:
    '''
    Paramètres :
        x : abcisse du centre de la façade
        y_sol : ordonnée du sol du la rue
        couleur : couleur de la façade
        niveau : num du niveau (0 pour les rdc, ...)
    '''
    t.color('black', couleur)
    t.begin_fill()
    rectangle(x, y_sol + HAUT_NIV * niveau, LARGEUR , HAUT_NIV)
    t.end_fill()

# ----- Toits -----

def toit1(x:Coord, y_sol:Coord, niveau:int) -> None:
    '''
    Toit 2 pentes
    Paramètres :
        x : abcisse du centre du toit
        y_sol : ordonnée du sol du la rue
        niveau : num du niveau (0 pour les rdc, ...)
    '''
    t.color('black')
    t.up()
    t.goto(x - 10, y_sol + HAUT_NIV * niveau)
    t.setheading(0)
    t.begin_fill()
    t.down()
    t.forward(LARGEUR + 20)
    t.left(150)
    t.forward(93)
    t.left(60)
    t.forward(93)
    t.end_fill()

def toit2(x:Coord, y_sol:Coord, niveau:int) -> None:
    '''
    Toit plat
    Paramètres :
        x : abcisse du centre du toit
        y_sol : ordonnée du sol du la rue
        niveau : num du niveau (0 pour les rdc, ...)
    '''
    t.color('black')
    t.up()
    t.goto(x - 10, y_sol + HAUT_NIV * niveau)
    t.setheading(0)
    t.begin_fill()
    t.down()
    for _ in range(2):
        t.forward(LARGEUR + 20)
        t.circle(5, 180)
    t.end_fill()

def toits() -> list:
    """
    Renvoie la liste des toits possibles
    """
    return [toit1, toit2]

# ----- RDC, Etage et Toit

def rdc(x:Coord, y_sol:Coord, c_facade:Couleur, c_porte: Couleur) -> None:
    '''
    Paramètres
        x : (int) abscisse du centre
        y_sol : ordonnée du sol du la rue
        c_facade : couleur de la façade
        c_porte : couleur de la porte
    '''
    facade(x, y_sol, couleur_aleatoire(), 0)
    p = 1
    f = 2
    for k in range(1, 4):
        m = randint(0, 1)
        if p > 0 and f > 0:
            z = randint(0, 1)
            if z == 1:
                if m == 0:
                    porte1((x - 70) + k/4 * LARGEUR, y_sol, c_porte)
                else:
                    porte2((x - 70) + k/4 * LARGEUR, y_sol, c_porte)  
                p = p - 1
            else:
                fenetre((x - 70) + k/4 * LARGEUR, y_sol + 20)
                f = f - 1
        elif f > 0 and p == 0:
            fenetre((x - 70) + k/4 * LARGEUR, y_sol + 20)
            f = f - 1
        elif p > 0 and f == 0:
            if m == 0:
                porte1((x - 70) + k/4 * LARGEUR, y_sol, c_porte)
            else:
                porte2((x - 70) + k/4 * LARGEUR, y_sol, c_porte) 
            p = p - 1

def etage(x:Coord, y_sol:Coord, couleur: Couleur, niveau: int) -> None:
    '''
    Paramètres
        x : abscisse du centre de l'étage
        y_sol : ordonnée du sol du la rue
        couleur : couleur de la façade de l'étage
        niveau : numéro de l'étage en partant de 0 pour le rdc
    '''
    facade(x, y_sol, couleur_aleatoire(), niveau)
    for k in range(1, 4):
        m = randint(0, 1)
        if m == 0:
            fenetre((x - 70) + k/4 * LARGEUR, y_sol + HAUT_NIV * niveau + 20)
        else:
            fenetre_balcon((x - 70) + k/4 * LARGEUR, y_sol + HAUT_NIV * niveau)

def toit(x:Coord, y_sol:Coord, niveau:int) -> None:
    '''
    Paramètres
        x : abscisse du centre de l'étage
        niveau : numéro de l'étage en partant de 0 pour le rdc
    '''
    g = randint(0, 1)
    if g == 0:
        toit1(x - LARGEUR/2, y_sol, niveau)
    else:
        toit2(x - LARGEUR/2, y_sol, niveau)

# ----- Immeuble -----

def immeuble(x:Coord, y_sol:Coord) -> None:
    '''
    Paramètres
        x : abscisse du centre de l'étage
        y_sol : ordonnée du sol du la rue
    '''
    # Nombre d'étage (aléatoire)
    nb_etages = randint(0, NIVEAU_MAX)
    #Couleurs des éléments (aléatoire)
    couleur_facade = couleur_aleatoire()
    couleur_porte = couleur_aleatoire()
    # Dessin du RDC
    rdc(x, y_sol, couleur_facade, couleur_porte)
    # Dessin des étages
    niveau = 1
    while niveau <= nb_etages:
        etage(x, y_sol, couleur_facade,niveau)
        niveau = niveau + 1
    # Dessin du toit
    toit(x, y_sol, niveau)

# ----- Sol de la rue -----
def sol(y_sol:Coord, nb_maisons:int) -> Coord:
    '''
    Paramètres
        y_sol : ordonnée du sol du la rue
    '''
    x_sol = LARGEUR//10 + (LARGEUR*1.2*nb_maisons)//2
    t.pensize(3)
    trait(-x_sol, y_sol, x_sol, y_sol)
    return x_sol



def dessin(nb_maisons:int) -> None:
    ini_tortue(nb_maisons)
    y_sol = -2*HAUT_NIV
    # Dessin du sol de la rue
    x_sol = sol(y_sol, nb_maisons)
    # Dessin des immeubles
    for i in range(nb_maisons):
        immeuble(LARGEUR*1.2//2 - x_sol + i*LARGEUR*1.2, y_sol)
    time.sleep(5)

# ------------------------------
# ------------------------------
# ------------------------------

#if __name__ == "__main__":
n = int(input("\n nombre de maisons ?  "))
dessin(n)

input()